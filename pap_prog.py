import os
import pickle
from time import sleep
from bs4 import BeautifulSoup
import gitlab
from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.core.utils import ChromeType
from os import path as p
# import geckodrivermanager


app_creds_dictionary = {
    "type": "service_account",
    "project_id": "selogercopy",
    "private_key_id": "595a2272dfb920381c1c7893638d66608f439899",
    # trunk-ignore(gitleaks/private-key)
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDlceFwgFY2xK5x\nxB/QvGCQLYAGTco3vE9KyOLvFVVT7vYgUOmq24fSTC1Fwdn5oinn/LGS7wslq4Vg\nj1cHsHQGi5kI9kK1yKHktamuec8JM+QOZOioCw7l9bg7ugix0kk/YiWyVzIoQ0TQ\nR3qcMTu1FOQBcK/W/np2r5S3lK8BJOY5JQA4HqDTOFikDps3kGA3pwC6Bk/jYyKh\neP1bx1mCbXNFlfdCkE7HYpL/1G6uCX9VY0C6f5mKl/HtCzZpW4S28JwsnteXvb6/\n7j2PUBSv3AFa1PqiLPLDlT9vZ19kOKm1JiSV3/oRV0mki8iW6EQINgMStJw/Vti2\naB5dgcG/AgMBAAECggEAJrSMyq0jng6UvOVQVqaIhZnfWHDj9X4DRELaIWnQYJ3V\nkEMGAAgTg/LMylE2w6lK93MFw5smnr6t+8mCbW4jTH1dHglo3ICRauMIUkIDHnLN\n/G2iX8Kpyzolz5GE6PZn29TjNhAGios6fwYn1VRq1ZPGGMYZK6P3JbFd3ZAr632r\nqb8RBc7a21hZIcN8BnTAjJ72I04x98HC5PBKEE+Jh+51fsobzyQzESaN41lA3Mxn\nrExQyCo6GfyROEVGkZJHsWM22QFutCj+ZkwsBs1PgfG/kFZDwVvk5Ob0FnwnIMC4\nFxVetErV7s7ZlaEbnUQMjzwulpoJN+HPYR2VwOsLXQKBgQD+RJga/bafX7EAOG8b\nl/1eIRDryX/EIbTErTWI6DWaZok88phYdYTFocGQAL1MXBMBv1MwYb0HpH/7ic9A\ngWDX5hZruqG3QBHLC2UbsoRo2cIVjl24fuxa3wcxkwYK2vaFI/pqCMYAL9GQxFfP\n9ivWoXDwW6HhzKi0CsKwd61yxQKBgQDnAf+mWemm1m8xv1og2/VZ4wGZpsE3dWlw\nouaoV2vGRsPLr38AaKn/juGaq3idkPPIMJURyGfue5KUaD9hOcfpRaEvgJrwxDvI\njx00655/nT9jG4MxbYhwsc/xySe9KByBrnbFMvpRGCP7zvKJ4EuRzrj9ZMBGOv1a\nf4MLunCaswKBgQDSEdfdDNpRh9yTgwyArqy8lPMG6t3tK6/Ogg/lwXtvyeD+gqtN\nAiKkqURi9clvie6GFgzjvwxOgSDfr3aUI4/gL488h9/Np1hL+WkaXf25JpeL+agC\nopIhbXvjAEYQt+DOFPBxpDf5tYgdY6ns/cQwOgc1/W6cP9rtjKpW4zlgxQKBgQDM\nF5LzOKrasMcqr4GEWHDrCklGo/I++ie+0N1yS1I3qhugIBvXk6Dl3SlrCiWVKBSJ\ngIqjXGieZWS6Y2PjlTYO3Wqr2jaJkwWyiuZl8+ljm9z9xbfh+oC5+A7c8jh+wSp2\nVFownXpUHTzlYxAbSiPZ3UbI8LODGOjvhIqV2RFVSwKBgQC8UcbnPeBMj52fyRZV\nQngVJoprhHd17eLg3QUIkdziY+CTIg84kdhKzGP/gsBbpos3bCZoWzp9MGxUg3pw\nuk/69c1aWuaWmImAAze5BxowBYfUkkDj1I54pishhN5L1r8ytlZGxkBCuNLL0QSn\ntrKnpouBfLmVwU7txvjKVdR5Ww==\n-----END PRIVATE KEY-----\n",
    "client_email": "selogercopy@selogercopy.iam.gserviceaccount.com",
    "client_id": "111687988114084841090",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/selogercopy%40selogercopy.iam.gserviceaccount.com"
}
scope =  [
    'https://spreadsheets.google.com/feeds',
    'https://www.googleapis.com/auth/drive'
]
TEXT_MESSAGE = "Je suis Peggy Sonego, responsable du cabinet BLG IMMOBILIER dédié aux investissements locatifs sur l’ensemble de l’Ile de France.  Que vous souhaitiez louer un logement soumis à une défiscalisation, en vide ou en meuble, je me ferais un plaisir de vous exposer de vive voix l’ensemble des services que nous proposons en gestion locative, ainsi que nos tarifs attractifs.\n\nPour cela, n’hésitez pas à me contacter, Peggy SONEGO/Cabinet BLG IMMOBILIER au 06 67 24 66 52 ou par mail à : contact@blgimmobilier.fr"
EMAIL = "locationbonlogis@gmail.com"
PHONE_NUMBER = "0145771633"
FIRST_NAME = "Peggy"
LAST_NAME = "Sonego"
LINK = "https://www.pap.fr/annonce/location-appartement-maison-paris-75-g439g441g456g457g458"

# force chrome to stop process
def forceStopChrome():
    os.system("taskkill /f /im chrome.exe")


def uploadFile(project_id, file_path, fileName):
    try:
        gl = gitlab.Gitlab('https://gitlab.com',
                           private_token="glpat-4mkyrDUnmLtzfcHsirZy")
        gl.auth()
        with open(file_path, 'r') as my_file:
            file_content = my_file.read()
        project = gl.projects.get(project_id)
        try:
            a = project.files.get(file_path=fileName, ref="main")
            f = project.files.update(file_path=fileName,
                                     new_data={
                                         'file_path': fileName,
                                         'branch': "main",
                                         'content': file_content,
                                         'author_email':
                                             "danhabib011@gmail.com",
                                         'author_name': "dd",
                                         'commit_message': 'Upload'
                                     })
        except Exception as e:
            f = project.files.create({
                'file_path': fileName,
                'branch': "main",
                'content': file_content,
                'author_email': "danhabib011@gmail.com",
                'author_name': "dd",
                'commit_message': 'Upload'
            })

        return True
    except Exception as e:
        return False


class SelogerCopy:
    def __init__(self):
        # proxies = [
        #     {'ip': '137.74.65.101', 'port': '80', 'country': 'France', 'city': 'Roubaix', 'speed': '149', 'protocol': 'http', 'anonymity': 'High', 'last_checked': '5 h.20 minutes'}
        # ]

        # proxy = random.choice(proxies)
        # print("Selected Proxy: ", proxy)

        # proxy_string = f"{proxy['ip']}:{proxy['port']}"

        chrome_options = Options()

        chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])
        chrome_options.add_argument("user-data-dir=" + os.getcwd() + "/pap")
        chrome_options.add_argument(
            "user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/58.0.3029.110 Safari/537.3")
        self.driver = webdriver.Chrome(ChromeDriverManager(chrome_type=ChromeType.GOOGLE).install(),
                                       options=chrome_options)

    def launchSite(self):
        self.driver.get(
            LINK)
        sleep(5)
        pickle.dump(self.driver.get_cookies(), open("pap.pkl", "wb"))

    def startCopy(self):
        try:
            self.driver.find_element_by_css_selector(
                "#sd-cmp > div.sd-cmp-2E0Ye > div > div > div > div > div > div > div.sd-cmp-WgGhS.sd-cmp-TVq-W > "
                "div:nth-child(2) > button:nth-child(2) > span").click()
        except Exception as e:
            pass
        sleep(1)
        i = 0
        a = 1
        fileNameName = "pathlink"
        fileName = os.getcwd() + f"/{fileNameName}.txt"
        # transform pathlink.txt to list
        with open(fileName, "r") as f:
            hrefToAvoid = f.read().splitlines()
        try:
            # input("Press Enter to start...")
            while True:
                hrefs = []
                page_source = BeautifulSoup(self.driver.page_source, "html.parser")
                divs = page_source.find_all("div", {"class": "col-1-3"})
                for div in divs:
                    try:
                        a = div.find("a", {"class": "item-thumb-link"})
                        if a is not None:
                            path = a.get("href")
                            if path not in hrefToAvoid:
                                hrefs.append(path)
                                self.driver.get(f"https://www.pap.fr{path}")
                                sleep(0.1)
                                try:

                                    # start writing message
                                    # clear before writing
                                    self.driver.find_element_by_css_selector("#sidebar_prenom").clear()
                                    self.driver.find_element_by_css_selector("#sidebar_prenom").send_keys(FIRST_NAME)

                                    self.driver.find_element_by_css_selector("#sidebar_nom").clear()
                                    self.driver.find_element_by_css_selector("#sidebar_nom").send_keys(LAST_NAME)

                                    self.driver.find_element_by_css_selector("#sidebar_email").clear()
                                    self.driver.find_element_by_css_selector("#sidebar_email").send_keys(EMAIL)

                                    self.driver.find_element_by_css_selector("#sidebar_telephone").clear()
                                    self.driver.find_element_by_css_selector("#sidebar_telephone").send_keys(PHONE_NUMBER)

                                    self.driver.find_element_by_css_selector("#sidebar_message").clear()
                                    self.driver.find_element_by_css_selector("#sidebar_message").send_keys(TEXT_MESSAGE)
                                    self.driver.find_element_by_css_selector(
                                        "body > div.sidebar-layout.margin-top-header.details-annonce-container > div > div.sidebar > div > form > div:nth-child(12) > input").click()
                                    try:
                                        # press esc key with selenium
                                        self.driver.find_element_by_xpath("/html/body/div[5]/div[2]/form/button").click()


                                    except Exception as e:
                                        pass
                                    sleep(0.5)
                                except Exception as e:
                                    pass
                    except Exception as e:
                        pass
                hrefToAvoid = hrefToAvoid + hrefs
                # write hrefstoavoid to pathlink.txt
                with open(fileName, "w") as f:
                    for item in hrefToAvoid:
                        f.write(item + "\n")
                
                if hrefs == []:
                    self.driver.get(LINK)
                self.driver.execute_script("window.scrollBy(0, 2800)")
            # # if element.get_attribute("target") != "_blank":
            #     # element.click()
            #     ActionChains(self.driver).move_to_element(element).click().perform()
            #     # self.actions.move_to_element(element).click().perform()

            #
            #     sleep(5)
        except Exception as e:
            print(e)
            pass
        i += 1
        # if a == 3 :
        #     a = 0
        # self.driver.execute_script("window.scrollBy(0, 100)")
        # else:
        #     a += 1
        # creds = ServiceAccountCredentials.from_json_keyfile_dict(
        #     app_creds_dictionary, scope)
        # client = gspread.authorize(creds)
        # sheet = client.open('selogercopy')
        # sheet_instance = sheet.get_worksheet(0)
        # sheet_instance.append_row(["test"])


# forceStopChrome()
app = SelogerCopy()
app.launchSite()
app.startCopy()
